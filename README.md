# How to run application

run `vagrant up` to prepare vagrant env

run `ansible-playbook -i inventories/vagrant foundation/run-app.yml` to run applications

In case of ssh problems run `ssh-keygen -f "/home/{user_name}/.ssh/known_hosts" -R 10.123.2.222`

Then connect to vagrant via `vagrant ssh` to connect to vagrant env

Now you can check output from applications

`sudo docker logs ping-service`

`sudo docker logs pong-service`
